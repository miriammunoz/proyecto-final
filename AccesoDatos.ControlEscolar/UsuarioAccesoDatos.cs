﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class UsuarioAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost","root","","Empresa",3309);
        }
        public void Guardar(Usuario usuario)//Objeto Usuario
        {
            if (usuario.IdUsuario==0)  //Insert
            {
                string consulta=string.Format("insert into Usuarios values(null,'{0}','{1}','{2}','{3}')", usuario.Nombre,usuario.ApellidoPaterno, usuario.ApellidoMaterno, usuario.Contraseña);
                conexion.EjecutarConsulta(consulta);
            }
            else   //Update
            {
                string consulta = string.Format("update Usuarios set Nombre='{0}', ApellidoPaterno='{1}', ApellidoMaterno='{2}', Contrasena='{3}' where idusuario={4}", usuario.Nombre, usuario.ApellidoPaterno, usuario.ApellidoMaterno, usuario.Contraseña, usuario.IdUsuario);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idUsuario)
        {
            //Eliminar
            string consulta = string.Format("delete from Usuarios where idusuario={0}",idUsuario);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = new List<Usuario>();

            var ds = new DataSet();
            string consulta = "select * from Usuarios where Nombre like '%"+filtro+"%'";
            ds = conexion.ObtenerDatos(consulta,"Usuarios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var usuario = new Usuario
                {
                    IdUsuario=Convert.ToInt32(row["idusuario"]),
                    Nombre=row["Nombre"].ToString(),
                    ApellidoPaterno=row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    Contraseña=row["Contrasena"].ToString()
                };
                listUsuario.Add(usuario);
            }

            //HardCodear: Ingresa datos directos para hacer prueba...
            /* listUsuario.Add(new Usuario {
                 IdUsuario = 1,
                 Nombre = "Nestor",
                 ApellidoPaterno="Chico",
                 ApellidoMaterno="Rojas"}); */

            //Llenar Lista
            return listUsuario;
        }
    }
}
