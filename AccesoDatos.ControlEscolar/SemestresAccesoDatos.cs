﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class SemestresAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public SemestresAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public List<Semestres> GetSemestres(string filtro)
        {
            var listSemestres = new List<Semestres>();

            var ds = new DataSet();
            string consulta = "select * from Semestres where NumeroSemestre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Semestres");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var semestres = new Semestres
                {
                    Idsemestre = Convert.ToInt32(row["idsemestre"]),
                    Numero = row["NumeroSemestre"].ToString(),

                };
                listSemestres.Add(semestres);
            }

            return listSemestres;

        }
    }
}
