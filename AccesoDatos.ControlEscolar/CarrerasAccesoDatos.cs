﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CarrerasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CarrerasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public List<Carreras> GetCarreras(string filtro)
        {
            var listCarreras = new List<Carreras>();

            var ds = new DataSet();
            string consulta = "select * from Carreras where NombreCarrera like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Carreras");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var carreras = new Carreras
                {
                    Idcarrera = Convert.ToInt32(row["idcarrera"]),
                    NombreCarrera = row["NombreCarrera"].ToString()

                };
                listCarreras.Add(carreras);
            }

            return listCarreras;

        }
    }
}
