﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;


namespace AccesoDatos.ControlEscolar
{
    public class AlumnosAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AlumnosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Alumnos alumnos)//Objeto Usuario
        {
            if (alumnos.Id == 0)  //Insert
            {
                string consulta = string.Format("insert into Alumnos values" +
                    "(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", 
                    alumnos.NumeroControl,alumnos.Nombre, alumnos.ApellidoPaterno, alumnos.ApellidoMaterno, 
                    alumnos.FechaNacimiento, alumnos.Sexo, alumnos.Domicilio, alumnos.Email, alumnos.FkCodigo, 
                    alumnos.FkCodigoCiudad);

                conexion.EjecutarConsulta(consulta);
            }
            else   //Update
            {
                string consulta = string.Format("update Alumnos set NumeroControl='{0}',Nombre='{1}'," +
                    " ApellidoPaterno='{2}', ApellidoMaterno='{3}', FechaNacimiento='{4}', Sexo='{5}'," +
                    " Domicilio='{6}',Email='{7}', fkCodigo='{8}', fkCodigoCiudad='{9}' where Id={10}", 
                    alumnos.NumeroControl, alumnos.Nombre, alumnos.ApellidoPaterno, alumnos.ApellidoMaterno, 
                    alumnos.FechaNacimiento, alumnos.Sexo, alumnos.Domicilio, alumnos.Email, alumnos.FkCodigo, 
                    alumnos.FkCodigoCiudad, alumnos.Id);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int Id)
        {
            string consulta = string.Format("delete from Alumnos where Id={0}", Id);
            conexion.EjecutarConsulta(consulta);
        }

        public Alumnos GetAlumnobyId(int id)
        {

            var ds = new DataSet();
            string consulta = string.Format("select * from Alumnos where Id = {0}",id);
            ds = conexion.ObtenerDatos(consulta, "Alumnos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            var alumnos = new Alumnos();

            foreach (DataRow row in dt.Rows)
            {
                 alumnos = new Alumnos
                {
                    Id = Convert.ToInt32(row["Id"]),
                    NumeroControl = Convert.ToInt32(row["NumeroControl"]),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    FechaNacimiento = row["FechaNacimiento"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    Email = row["Email"].ToString(),
                    FkCodigo = row["fkCodigo"].ToString(),
                    FkCodigoCiudad = Convert.ToInt32(row["fkCodigoCiudad"])
                };             
            }
            return alumnos;

        }
       
        public List<VistaAlumnos> GetAlumnos(string filtro)
        {
            var listvAlumnos = new List<VistaAlumnos>();

            var ds = new DataSet();
            string consulta = "select * from v_alumnos where Nombrealumno like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "v_alumnos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var valumnos = new VistaAlumnos
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombrealumno = row["nombrealumno"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanacimiento = row["fechanacimiento"].ToString(),
                    Sexo=row["sexo"].ToString(),
                    Domicilio=row["domicilio"].ToString(),
                    Email=row["email"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString()
                    

                };
                listvAlumnos.Add(valumnos);
              
            }
            return listvAlumnos;
        }

    }
}
