﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GruposAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public GruposAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Grupos grupos)
        {
            if (grupos.Idgrupo == 0)
            {
                string consulta = string.Format("insert into Grupos values" +
                    "(null,'{0}','{1}','{2}','{3}')",
                    grupos.Semestre, grupos.Grupo, grupos.Carrera, grupos.Alumno);

                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update Grupos set fkidsemestre='{0}',NombreGrupo='{1}', fkidcarrera='{2}', fkidalumno='{3}' where idgrupo={4}",
                 grupos.Semestre, grupos.Grupo, grupos.Carrera, grupos.Alumno, grupos.Idgrupo);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from Grupos where idgrupo={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<VistaGrupo> GetGrupos(string filtro)
        {
            var listvGrupo = new List<VistaGrupo>();

            var ds = new DataSet();
            string consulta = "select* from v_Grupo where Grupo like '%" + filtro + "%'";;
            ds = conexion.ObtenerDatos(consulta, "v_Grupo");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var vgrupo = new VistaGrupo
                {   
                    Idgrupo=Convert.ToInt32(row["idgrupo"]),
                    Grupo = row["Grupo"].ToString(),
                    Carrera = row["Carrera"].ToString(),
                    NumeroControl=row["NumeroControl"].ToString(),
                    Alumno = row["Alumno"].ToString()
                };
                listvGrupo.Add(vgrupo);
            }
            return listvGrupo;
        }
    }
}
