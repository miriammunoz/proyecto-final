﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AsignacionesAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AsignacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Asignaciones asignaciones)
        {
            if (asignaciones.Idasignacion == 0)
            {
                string consulta = string.Format("insert into Asignaciones values" +
                    "(null,'{0}','{1}','{2}')", asignaciones.Maestro, asignaciones.Grupo, asignaciones.Materia);

                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update Asignaciones set fkidmaestro='{0}',fkidgrupo='{1}', fkidmateria='{2}' where idasignacion={3}",
                 asignaciones.Maestro, asignaciones.Grupo, asignaciones.Materia, asignaciones.Idasignacion);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from Asignaciones where idasignacion={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Asignaciones> GetAsignaciones(string filtro)
        {
            var listAsignaciones = new List<Asignaciones>();

            var ds = new DataSet();
            string consulta = "select idasignacion, Maestros.Nombre AS 'Maestro', Grupos.fkidsemestre AS 'Semestre', NombreGrupo AS 'Grupo' , NombreMateria AS 'Materia' from Maestros, Grupos, Materias, Asignaciones where Asignaciones.fkidmaestro = Maestros.idmaestro and Asignaciones.fkidgrupo = Grupos.idgrupo and Asignaciones.fkidmateria = Materias.idmateria and Maestros.Nombre like '%" + filtro + "%';";
            ds = conexion.ObtenerDatos(consulta, "Asignaciones");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asignaciones = new Asignaciones
                {
                    Idasignacion = Convert.ToInt32(row["idasignacion"]),
                    Maestro = row["Maestro"].ToString(),
                    Semestre = row["Semestre"].ToString(),
                    Grupo = row["Grupo"].ToString(),
                    Materia = row["Materia"].ToString(),
                };
                listAsignaciones.Add(asignaciones);
            }
            return listAsignaciones;
        }
    }
}
