﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public MateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Materias materias)
        {
            if (materias.Idmateria == 0)
            {
                string consulta = string.Format("insert into Materias values" +
                    "(null,'{0}','{1}','{2}','{3}','{4}','{5}')",
                    materias.Codigo, materias.NombreMateria, materias.HoraTeoria, materias.HoraPractica,
                    materias.Semestre, materias.Carrera);

                conexion.EjecutarConsulta(consulta);
            }
            else 
            {
                string consulta = string.Format("update Materias set Codigo='{0}',NombreMateria='{1}',HoraTeoria='{2}', HoraPractica='{3}', fkidsemestre='{4}', fkidcarrera='{5}' where idmateria={6}",
                    materias.Codigo, materias.NombreMateria, materias.HoraTeoria, materias.HoraPractica,
                    materias.Semestre, materias.Carrera, materias.Idmateria);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int Id)
        {
            string consulta = string.Format("delete from Materias where idmateria={0}", Id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Materias> GetMaterias(string filtro)
        {
            var listMaterias = new List<Materias>();

            var ds = new DataSet();
            //string consulta = "select * from Maestros where Nombre like '%" + filtro + "%'";
            string consulta = "select idmateria as 'Id', Codigo, NombreMateria, HoraTeoria, HoraPractica, (HoraTeoria + HoraPractica) Creditos, NumeroSemestre AS 'Semestre', NombreCarrera AS 'Carrera' from Materias, Semestres, Carreras where Materias.fkidsemestre = Semestres.idsemestre and Materias.fkidcarrera = Carreras.idcarrera and NombreMateria like '%" + filtro + "%';";
            ds = conexion.ObtenerDatos(consulta, "Materias");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materias = new Materias
                {
                    Idmateria = Convert.ToInt32(row["Id"]),
                    Codigo = row["Codigo"].ToString(),
                    NombreMateria = row["NombreMateria"].ToString(),
                    HoraTeoria = Convert.ToInt32(row["HoraTeoria"]),
                    HoraPractica = Convert.ToInt32(row["HoraPractica"]),
                    Creditos=Convert.ToInt32(row["Creditos"]),
                    Semestre = row["Semestre"].ToString(),
                    Carrera = row["Carrera"].ToString()
                };

                listMaterias.Add(materias);
            }
            return listMaterias;
        }
    }
}
