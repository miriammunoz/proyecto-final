﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
     public class CiudadesAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CiudadesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public List<Ciudades> GetCiudades(string filtro)
        {
            var listCiudades = new List<Ciudades>();

            var ds = new DataSet();
            string consulta = "select * from Ciudades where fkCodigo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Ciudades");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Ciudades
                {
                    CodigoCiudad = Convert.ToInt32(row["CodigoCiudad"]),
                    NombreCiudad = row["NombreCiudad"].ToString(),
                    fkCodigo = row["fkCodigo"].ToString()

                };
                listCiudades.Add(ciudades);
            }

            return listCiudades;

        }
    }
}
