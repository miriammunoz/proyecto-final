﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;
using MySql.Data.MySqlClient;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaAccesoDatos
    {
   
        ConexionAccesoDatos conexion;
        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Escuela escuela)
        {
            if (escuela.Id == 0)
            {
                string consulta = string.Format("insert into Escuela values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", escuela.Nombre, escuela.Rfc, escuela.Domicilio, escuela.Telefono, escuela.Paginaweb, escuela.Correo, escuela.Director, escuela.Logo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update Escuela set Nombre='{0}', RFC='{1}', Domicilio='{2}', Telefono='{3}', PaginaWeb='{4}', Correo='{5}', Director='{6}', Logo='{7}' where idescuela={8}", escuela.Nombre, escuela.Rfc, escuela.Domicilio, escuela.Telefono, escuela.Paginaweb, escuela.Correo, escuela.Director, escuela.Logo, escuela.Id);
                conexion.EjecutarConsulta(consulta);
            }
        }

        /*public List<Escuela> GetEscuela()
        {
            var listEscuela = new List<Escuela>();

            var ds = new DataSet();
            string consulta = "select * from Escuela where idescuela=1";
            ds = conexion.ObtenerDatos(consulta, "Escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];

          
            MySqlCommand myCmn = new MySqlCommand();
            MySqlDataReader myReader = myCmn.ExecuteReader();
            
            if (myReader.Read())
            {
                var escuela = new Escuela
                {
                    Id = Convert.ToInt32(myReader["idescuela"]),
                    Nombre = myReader["Nombre"].ToString(),
                    Rfc = myReader["RFC"].ToString(),
                    Domicilio = myReader["Domicilio"].ToString(),
                    Telefono = myReader["Telefono"].ToString(),
                    Paginaweb = myReader["PaginaWeb"].ToString(),
                    Correo = myReader["Correo"].ToString(),
                    Director = myReader["Director"].ToString(),
                    Logo = myReader["Logo"].ToString()
                    
                };
                listEscuela.Add(escuela);
            }
            return listEscuela;
        }*/
       /* public List<Escuela> GetEscuela(string filtro)
        {
            var listEscuela = new List<Escuela>();

            var ds = new DataSet();
            string consulta = "select * from Escuela where Nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];

            
            foreach (DataRow row in dt.Rows)
            {
                
                var escuela = new Escuela
                {
                    Id = Convert.ToInt32(row["idescuela"]),
                    Nombre = row["Nombre"].ToString(),
                    Rfc = row["RFC"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Paginaweb= row["PaginaWeb"].ToString(),
                    Correo=row["Correo"].ToString(),
                    Director=row["Director"].ToString(),
                    Logo=row["Logo"].ToString()
                };
                listEscuela.Add(escuela);
            }     
            return listEscuela;
        }*/
        public DataSet Registro()
        {
            var rg = new DataSet();
            string consulta = string.Format("select * from Escuela");
            rg = conexion.ObtenerDatos(consulta, "Escuela");
            return rg;
        }
    }
}
