﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AltaMateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AltaMateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(AltaMaterias Amaterias)
        {
            if (Amaterias.Idalta == 0)
            {
                string consulta = string.Format("insert into AltaMaterias values" +
                    "(null,'{0}','{1}','{2}','{3}','{4}','{5}')", Amaterias.Alumno, Amaterias.Materia, Amaterias.Parcial1, Amaterias.Parcial2, Amaterias.Parcial3, Amaterias.Parcial4);

                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update AltaMaterias set fkidalumno='{0}',fkidmateria='{1}',Parcial1='{2}',Parcial2='{3}',Parcial3='{4}',Parcial4='{5}' where idaltamaterias={6}",
                 Amaterias.Alumno, Amaterias.Materia, Amaterias.Parcial1, Amaterias.Parcial2, Amaterias.Parcial3, Amaterias.Parcial4, Amaterias.Idalta);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from AltaMaterias where idaltamaterias={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<AltaMaterias> GetAlta(string filtro)
        {
            var listAlta = new List<AltaMaterias>();

            var ds = new DataSet();
            string consulta = "select idaltamaterias, NumeroControl, Alumnos.Nombre AS 'Alumno', NombreMateria AS 'Materia', Parcial1, Parcial2, Parcial3, Parcial4, ((Parcial1+parcial2+parcial3+parcial4)/4) AS 'Promedio' from Alumnos, Materias, AltaMaterias where AltaMaterias.fkidalumno = Alumnos.Id and AltaMaterias.fkidmateria = Materias.idmateria and Alumnos.Nombre like '%" + filtro + "%';";
            ds = conexion.ObtenerDatos(consulta, "AltaMateria");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var Amateria = new AltaMaterias
                {
                    Idalta = Convert.ToInt32(row["idaltamaterias"]),
                    NumeroControl = row["NumeroControl"].ToString(),
                    Alumno = row["Alumno"].ToString(),
                    Materia = row["Materia"].ToString(),
                    Parcial1=Convert.ToDouble(row["Parcial1"]),
                    Parcial2 = Convert.ToDouble(row["Parcial2"]),
                    Parcial3 = Convert.ToDouble(row["Parcial3"]),
                    Parcial4 = Convert.ToDouble(row["Parcial4"]),
                    Promedio = Convert.ToDouble(row["Promedio"]),

                };
                listAlta.Add(Amateria);
            }
            return listAlta;
        }
    }
}
