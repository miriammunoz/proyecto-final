﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstudiosAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public EstudiosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }

        public void Guardar(Estudios estudios)
        {
            if (estudios.Id == 0)  
            {
                string consulta = string.Format("insert into Estudios values(null,'{0}','{1}','{2}','{3}')", estudios.Fkidmaestro, estudios.Cedula, estudios.Titulo,estudios.Documento);
                conexion.EjecutarConsulta(consulta);
            }
            else   
            {
                string consulta = string.Format("update Estudios set fkidmaestro='{0}',Cedula='{1}',Titulo='{2}', Documento='{3}' where id={4}", estudios.Fkidmaestro, estudios.Cedula, estudios.Titulo, estudios.Documento, estudios.Id);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from Estudios where id={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public Estudios GetEstudiosbyId(int id)
        {
            var ds = new DataSet();
            string consulta = string.Format("select * from Estudios where Id = {0}", id);
            ds = conexion.ObtenerDatos(consulta, "Estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            var estudios = new Estudios();

            foreach (DataRow row in dt.Rows)
            {
                estudios = new Estudios
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Cedula = row["Cedula"].ToString(),
                    Titulo = row["Titulo"].ToString(),                  
                    Documento = row["Documento"].ToString(),
                    Fkidmaestro = Convert.ToInt32(row["fkidmaestro"])
                };
            }
            return estudios;
        }

        public List<VistaEstudios> GetEstudios(string filtro)
        {
            var listvEstudios = new List<VistaEstudios>();

            var ds = new DataSet();
            string consulta = "select * from v_estudios where Cedula like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "v_estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var vestudios = new VistaEstudios
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Numerocontrol=row["NumeroControl"].ToString(),
                    Maestro = row["Maestro"].ToString(),
                    Apellidopaterno=row["ApellidoPaterno"].ToString(),
                    Cedula = row["Cedula"].ToString(),                   
                    Titulo = row["Titulo"].ToString(),
                    Documento = row["Documento"].ToString(),

                };
                listvEstudios.Add(vestudios);

            }
            return listvEstudios;
        }

    }
}
