﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class Materias
    {
        private int _idmateria;
        private string _Codigo;
        private string _NombreMateria;
        private int _HoraTeoria;
        private int _HoraPractica;
        private int _Creditos;
        private string _Semestre;
        private string _Carrera;

        public int Idmateria { get => _idmateria; set => _idmateria = value; }
        public string Codigo { get => _Codigo; set => _Codigo = value; }
        public string NombreMateria { get => _NombreMateria; set => _NombreMateria = value; }
        public string Semestre { get => _Semestre; set => _Semestre = value; }
        public string Carrera { get => _Carrera; set => _Carrera = value; }
        public int HoraTeoria { get => _HoraTeoria; set => _HoraTeoria = value; }
        public int HoraPractica { get => _HoraPractica; set => _HoraPractica = value; }
        public int Creditos { get => _Creditos; set => _Creditos = value; }
    }
}
