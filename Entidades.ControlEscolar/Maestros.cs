﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Maestros
    {
        private int _idmaestro;
        private string _numerocontrol;
        private string _Nombre;
        private string _ApellidoPaterno;
        private string _ApellidoMaterno;
        private string _FechaNacimiento;
        private string _Direccion;
        private string _Estado;
        private string _Ciudad;

        public int Idmaestro { get => _idmaestro; set => _idmaestro = value; }
        public string Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string ApellidoPaterno { get => _ApellidoPaterno; set => _ApellidoPaterno = value; }
        public string ApellidoMaterno { get => _ApellidoMaterno; set => _ApellidoMaterno = value; }
        public string FechaNacimiento { get => _FechaNacimiento; set => _FechaNacimiento = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Ciudad { get => _Ciudad; set => _Ciudad = value; }
    }
}
