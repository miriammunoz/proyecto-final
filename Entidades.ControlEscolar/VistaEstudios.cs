﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class VistaEstudios
    {
        private int _id;
        private string _numerocontrol;
        private string _maestro;
        private string _apellidopaterno;
        private string _cedula;
        private string _titulo;
        private string _documento;

        public int Id { get => _id; set => _id = value; }
        public string Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
        public string Maestro { get => _maestro; set => _maestro = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Cedula { get => _cedula; set => _cedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Documento { get => _documento; set => _documento = value; }
    }
}
