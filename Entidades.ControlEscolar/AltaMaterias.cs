﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class AltaMaterias
    {
        private int _idalta;
        private string _NumeroControl;
        private string _Alumno;
        private string _Materia;
        private double _parcial1;
        private double _parcial2;
        private double _parcial3;
        private double _parcial4;
        private double _promedio;

        public int Idalta { get => _idalta; set => _idalta = value; }
        public string NumeroControl { get => _NumeroControl; set => _NumeroControl = value; }
        public string Alumno { get => _Alumno; set => _Alumno = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
        public double Parcial1 { get => _parcial1; set => _parcial1 = value; }
        public double Parcial2 { get => _parcial2; set => _parcial2 = value; }
        public double Parcial3 { get => _parcial3; set => _parcial3 = value; }
        public double Parcial4 { get => _parcial4; set => _parcial4 = value; }
        public double Promedio { get => _promedio; set => _promedio = value; }
    }
}
