﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class Ciudades
    {
        private int _CodigoCiudad;
        private string _NombreCiudad;
        private string _fkCodigo;

        public int CodigoCiudad { get => _CodigoCiudad; set => _CodigoCiudad = value; }
        public string NombreCiudad { get => _NombreCiudad; set => _NombreCiudad = value; }
        public string fkCodigo { get => _fkCodigo; set => _fkCodigo = value; }
    }
}
