﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Grupos
    {
        private int _idgrupo;
        private string _Semestre;
        private string _Grupo;
        private string _Carrera;
        private string _Alumno;

        public int Idgrupo { get => _idgrupo; set => _idgrupo = value; }
        public string Semestre { get => _Semestre; set => _Semestre = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Carrera { get => _Carrera; set => _Carrera = value; }
        public string Alumno { get => _Alumno; set => _Alumno = value; }
    }
}
