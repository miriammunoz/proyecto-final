﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class Alumnos
    {

        private int _Id;
        private int _NumeroControl;
        private string _Nombre;
        private string _ApellidoPaterno;
        private string _ApellidoMaterno;
        private string _FechaNacimiento;
        private string _Sexo;
        private string _Domicilio;
        private string _Email;
        private string _fkCodigo;
        private int _fkCodigoCiudad;

        public int Id { get => _Id; set => _Id = value; }
        public int NumeroControl { get => _NumeroControl; set => _NumeroControl = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string ApellidoPaterno { get => _ApellidoPaterno; set => _ApellidoPaterno = value; }
        public string ApellidoMaterno { get => _ApellidoMaterno; set => _ApellidoMaterno = value; }
        public string FechaNacimiento { get => _FechaNacimiento; set => _FechaNacimiento = value; }
        public string Sexo { get => _Sexo; set => _Sexo = value; }
        public string Domicilio { get => _Domicilio; set => _Domicilio = value; }
        public string Email { get => _Email; set => _Email = value; }
        public string FkCodigo { get => _fkCodigo; set => _fkCodigo = value; }
        public int FkCodigoCiudad { get => _fkCodigoCiudad; set => _fkCodigoCiudad = value; }
    }
}
