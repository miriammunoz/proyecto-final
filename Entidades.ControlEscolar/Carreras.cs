﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Carreras
    {
        private int _idcarrera;
        private string _NombreCarrera;

        public int Idcarrera { get => _idcarrera; set => _idcarrera = value; }
        public string NombreCarrera { get => _NombreCarrera; set => _NombreCarrera = value; }
    }
}
