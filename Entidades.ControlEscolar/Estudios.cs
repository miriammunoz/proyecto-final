﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Estudios
    {
        private int _id;
        private string _cedula;
        private string _titulo;
        private string _documento;
        private int _fkidmaestro;

        public int Id { get => _id; set => _id = value; }
        public string Cedula { get => _cedula; set => _cedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Documento { get => _documento; set => _documento = value; }
        public int Fkidmaestro { get => _fkidmaestro; set => _fkidmaestro = value; }
    }
}
