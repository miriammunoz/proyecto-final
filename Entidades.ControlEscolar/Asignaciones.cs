﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Asignaciones
    {
        private int _Idasignacion;
        private string _Maestro;
        private string _Semestre;
        private string _Grupo;
        private string _Materia;

        public int Idasignacion { get => _Idasignacion; set => _Idasignacion = value; }
        public string Maestro { get => _Maestro; set => _Maestro = value; }
        public string Semestre { get => _Semestre; set => _Semestre = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
      
    }
}
