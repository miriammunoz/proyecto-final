﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Semestres
    {
        private int _idsemestre;
        private string _Numero;

        public int Idsemestre { get => _idsemestre; set => _idsemestre = value; }
        public string Numero { get => _Numero; set => _Numero = value; }
    }
}
