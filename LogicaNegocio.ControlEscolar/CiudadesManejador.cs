﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CiudadesManejador
    {
        private CiudadesAccesoDatos _CiudadesAccesoDatos = new CiudadesAccesoDatos();
        public List<Ciudades> GetCiudades(string filtro)
        {
            var listCiudades = _CiudadesAccesoDatos.GetCiudades(filtro);

            //Llenar Lista
            return listCiudades;
        }
    }
}