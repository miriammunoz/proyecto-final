﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CarrerasManejador
    {
        private CarrerasAccesoDatos _CarrerasAccesoDatos = new CarrerasAccesoDatos();
        public List<Carreras> GetCarreras(string filtro)
        {
            var listCarreras = _CarrerasAccesoDatos.GetCarreras(filtro);

            //Llenar Lista
            return listCarreras;
        }
    }
}
