﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class SemestresManejador
    {
        private SemestresAccesoDatos _SemestresAccesoDatos = new SemestresAccesoDatos();
        public List<Semestres> GetSemestres(string filtro)
        {
            var listSemestres = _SemestresAccesoDatos.GetSemestres(filtro);

            //Llenar Lista
            return listSemestres;
        }
    }
}
