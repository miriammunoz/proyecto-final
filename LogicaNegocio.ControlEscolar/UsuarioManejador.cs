﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _UsuarioAccesoDatos = new UsuarioAccesoDatos();

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool ApellidoValido(string apellido)
        {
            var regex = new Regex(@"^[A-Za-z]*$");
            var match = regex.Match(apellido);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        public Tuple<bool,string> ValidarUsuario(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;
            
            //Nombre
            if (usuario.Nombre.Length==0)
            {
                mensaje = mensaje + "El Nombre de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.Nombre.Length>20)
            {
                mensaje = mensaje + "El Nombre de usuario solo permite un máximo de 20 caracteres \n";
                valido = false;
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre de usuario \n";
                valido = false;
            }


            //Apellido Paterno
            if (usuario.ApellidoPaterno.Length == 0)
            {
                mensaje = mensaje + "El Apellido Paterno de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length > 20)
            {
                mensaje = mensaje + "El Apellido Paterno de usuario solo permite un máximo de 20 caracteres \n";
                valido = false;
            }
            else if (!ApellidoValido(usuario.ApellidoPaterno))
            {
                mensaje = mensaje + "Escribe un formato valido para el Apellido Paterno de usuario \n";
                valido = false;
            }


            //Apellido Materno
            if (usuario.ApellidoMaterno.Length == 0)
            {
                mensaje = mensaje + "El Apellido Materno de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.ApellidoMaterno.Length > 20)
            {
                mensaje = mensaje + "El Apellido Materno de usuario solo permite un máximo de 20 caracteres \n";
                valido = false;
            }
            else if (!ApellidoValido(usuario.ApellidoMaterno))
            {
                mensaje = mensaje + "Escribe un formato valido para el Apellido Materno de usuario \n";
                valido = false;
            }


            return Tuple.Create(valido, mensaje);
        }
        public void Guardar(Usuario usuario)//Objeto Usuario
        {
            _UsuarioAccesoDatos.Guardar(usuario);
        }
        public void Eliminar(int idUsuario)
        {
            _UsuarioAccesoDatos.Eliminar(idUsuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            var listUsuario = _UsuarioAccesoDatos.GetUsuarios(filtro);

            //Llenar Lista
            return listUsuario;
        }
    }
}
