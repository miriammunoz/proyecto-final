﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _EscuelaAccesoDatos = new EscuelaAccesoDatos();
        public void Guardar(Escuela escuela)
        {
            _EscuelaAccesoDatos.Guardar(escuela);
        }
        /*public List<Escuela> GetEscuela(string filtro)
        {
            var listEscuela = _EscuelaAccesoDatos.GetEscuela(filtro);

            return listEscuela;
        }
        */
        public DataSet Registro()
        {
            return _EscuelaAccesoDatos.Registro();
        }
    }
}
