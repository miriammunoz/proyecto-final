﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _MateriasAccesoDatos = new MateriasAccesoDatos();
        public void Guardar(Materias materias)
        {
            _MateriasAccesoDatos.Guardar(materias);
        }
        public void Eliminar(int Id)
        {
            _MateriasAccesoDatos.Eliminar(Id);
        }
        public List<Materias> GetMaterias(string filtro)
        {
            var listMaterias = _MateriasAccesoDatos.GetMaterias(filtro);

            return listMaterias;
        }
    }
}
