﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstudiosManejador
    {
        private EstudiosAccesoDatos _EstudiosAccesoDatos = new EstudiosAccesoDatos();
        public void Guardar(Estudios estudios)
        {
            _EstudiosAccesoDatos.Guardar(estudios);
        }
        public void Eliminar(int id)
        {
            _EstudiosAccesoDatos.Eliminar(id);
        }
        public List<VistaEstudios> GetEstudios(string filtro)
        {
            var listvEstudios = _EstudiosAccesoDatos.GetEstudios(filtro);

            return listvEstudios;
        }
        public Estudios GetEstudiosbyId(int id)
        {
            return _EstudiosAccesoDatos.GetEstudiosbyId(id);

        }


    }
}
