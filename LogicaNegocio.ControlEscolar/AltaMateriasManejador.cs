﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AltaMateriasManejador
    {
        private AltaMateriasAccesoDatos _AltaMateriasAccesoDatos = new AltaMateriasAccesoDatos();
        public void Guardar(AltaMaterias Amaterias)
        {
            _AltaMateriasAccesoDatos.Guardar(Amaterias);
        }
        public void Eliminar(int Id)
        {
            _AltaMateriasAccesoDatos.Eliminar(Id);
        }
        public List<AltaMaterias> GetAlta(string filtro)
        {
            var listAlta = _AltaMateriasAccesoDatos.GetAlta(filtro);

            return listAlta;
        }
    }
}
