﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AsignacionesManejador
    {
        private AsignacionesAccesoDatos _AsignacionesAccesoDatos = new AsignacionesAccesoDatos();
        public void Guardar(Asignaciones asignaciones)
        {
            _AsignacionesAccesoDatos.Guardar(asignaciones);
        }
        public void Eliminar(int Id)
        {
            _AsignacionesAccesoDatos.Eliminar(Id);
        }
        public List<Asignaciones> GetAsignaciones(string filtro)
        {
            var listAsignaciones = _AsignacionesAccesoDatos.GetAsignaciones(filtro);

            return listAsignaciones;
        }
    }
}
