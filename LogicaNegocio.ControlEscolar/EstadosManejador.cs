﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos  _EstadosAccesoDatos = new EstadosAccesoDatos();
        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = _EstadosAccesoDatos.GetEstados(filtro);

            //Llenar Lista
            return listEstados;
        }
    }
}
