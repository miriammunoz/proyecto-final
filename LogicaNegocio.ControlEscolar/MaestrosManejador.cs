﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class MaestrosManejador
    {
        private MaestrosAccesoDatos _MaestrosAccesoDatos = new MaestrosAccesoDatos();
        public void Guardar(Maestros maestros)
        {
            _MaestrosAccesoDatos.Guardar(maestros);
        }
        public void Eliminar(int Id)
        {
            _MaestrosAccesoDatos.Eliminar(Id);
        }
        public List<Maestros> GetMaestros(string filtro)
        {
            var listMaestros = _MaestrosAccesoDatos.GetMaestros(filtro);

            return listMaestros;
        }
        public DataSet NumeroControl(string i)
        {
            return _MaestrosAccesoDatos.NumeroControl(i);
        }
    }
}
