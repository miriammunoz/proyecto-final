﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GruposManejador
    {
        private GruposAccesoDatos _GruposAccesoDatos = new GruposAccesoDatos();
        public void Guardar(Grupos grupos)
        {
            _GruposAccesoDatos.Guardar(grupos);
        }
        public void Eliminar(int Id)
        {
            _GruposAccesoDatos.Eliminar(Id);
        }
        public List<VistaGrupo> GetGrupos(string filtro)
        {
            var listGrupos = _GruposAccesoDatos.GetGrupos(filtro);

            return listGrupos;
        }
    }
}
