﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AlumnoManejador
    {
        private AlumnosAccesoDatos _AlumnosAccesoDatos = new AlumnosAccesoDatos();
        public void Guardar(Alumnos alumnos)//Objeto Usuario
        {
            _AlumnosAccesoDatos.Guardar(alumnos);
        }
        public void Eliminar(int Id)
        {
            _AlumnosAccesoDatos.Eliminar(Id);
        }
        public List<VistaAlumnos> GetAlumnos(string filtro)
        {
            var listvAlumnos = _AlumnosAccesoDatos.GetAlumnos(filtro);

            //Llenar Lista
            return listvAlumnos;
        }
        public Alumnos GetAlumnobyId(int id)
        {
            return _AlumnosAccesoDatos.GetAlumnobyId(id);

        }
    }
}
