﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmMaterias : Form
    {
        private MateriasManejador _MateriasManejador;
        private SemestresManejador _SemestresManejador;
        private CarrerasManejador _CarrerasManejador;

        public frmMaterias()
        {
            InitializeComponent();
            _MateriasManejador = new MateriasManejador();
            _SemestresManejador = new SemestresManejador();
            _CarrerasManejador = new CarrerasManejador();
        }
        private void frmMaterias_Load(object sender, EventArgs e)
        {
            BuscarMateria("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerSemestre("");
            TraerCarrera("");
        }
        private void BuscarMateria(string filtro)
        {
            dgvMaterias.DataSource = _MateriasManejador.GetMaterias(filtro);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMateria(txtBuscar.Text);
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtCodigo.Enabled = activar;
            txtMateria.Enabled = activar;
            txtHT.Enabled = activar;
            txtHP.Enabled = activar;
            cbxSemestre.Enabled = activar;
            cbxCarrera.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtCodigo.Text = "";
            txtMateria.Text = "";
            lblId.Text = "0";
            txtHT.Text = "";
            txtHP.Text = "";
            cbxCarrera.Text = "";
            cbxSemestre.Text = "";
        }
        private void TraerSemestre(string filtro)
        {
            cbxSemestre.DataSource = _SemestresManejador.GetSemestres(filtro);        
            cbxSemestre.ValueMember = "idsemestre";
            cbxSemestre.DisplayMember = "NumeroSemestre";
        }
        private void TraerCarrera(string filtro)
        {
            cbxCarrera.DataSource = _CarrerasManejador.GetCarreras(filtro);
            cbxCarrera.DisplayMember = "NombreCarrera";
            cbxCarrera.ValueMember = "idcarrera";
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarMateria();
                LimpiarCuadros();
                BuscarMateria("");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarMateria()
        {
            _MateriasManejador.Guardar(new Materias
            {
                Idmateria = Convert.ToInt32(lblId.Text),
                Codigo = txtCodigo.Text,
                NombreMateria = txtMateria.Text,
                HoraTeoria = Convert.ToInt32(txtHT.Text),
                HoraPractica = Convert.ToInt32(txtHP.Text),
                Semestre = cbxSemestre.SelectedValue.ToString(),
                Carrera = cbxCarrera.SelectedValue.ToString(),

            });
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMateria();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarMateria()
        {
            var Id = dgvMaterias.CurrentRow.Cells["idmateria"].Value;
            _MateriasManejador.Eliminar(Convert.ToInt32(Id));
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void dgvMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMaterias();
                BuscarMateria("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarMaterias()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            lblId.Text = dgvMaterias.CurrentRow.Cells["idmateria"].Value.ToString();
            txtCodigo.Text = dgvMaterias.CurrentRow.Cells["Codigo"].Value.ToString();
            txtMateria.Text = dgvMaterias.CurrentRow.Cells["NombreMateria"].Value.ToString();
            txtHT.Text = dgvMaterias.CurrentRow.Cells["HoraTeoria"].Value.ToString();
            txtHP.Text = dgvMaterias.CurrentRow.Cells["HoraPractica"].Value.ToString();
            cbxSemestre.Text = dgvMaterias.CurrentRow.Cells["Semestre"].Value.ToString();
            cbxCarrera.Text = dgvMaterias.CurrentRow.Cells["Carrera"].Value.ToString();

        }


    }

}

