﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmAsignaciones : Form
    {
        private AsignacionesManejador _AsignacionesManejador;
        private GruposManejador _GruposManejador;
        private MaestrosManejador _MaestrosManejador;
        private MateriasManejador _MateriasManejador;

        public frmAsignaciones()
        {
            InitializeComponent();
            _AsignacionesManejador = new AsignacionesManejador();
            _GruposManejador = new GruposManejador();
            _MaestrosManejador = new MaestrosManejador();
            _MateriasManejador = new MateriasManejador();
        }

        private void frmAsignaciones_Load(object sender, EventArgs e)
        {
            BuscarAsignacion("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerMaestro("");
            TraerGrupo("");
            TraerMateria("");
        }
        private void BuscarAsignacion(string filtro)
        {
            dgvAsignaciones.DataSource = _AsignacionesManejador.GetAsignaciones(filtro);
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cbxMaestro.Enabled = activar;
            cbxGrupo.Enabled = activar;
            cbxMateria.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            cbxMateria.Text = "";
            cbxMaestro.Text = "";
            cbxGrupo.Text = "";
        }
        private void TraerMaestro(string filtro)
        {
            cbxMaestro.DataSource = _MaestrosManejador.GetMaestros(filtro);
            cbxMaestro.DisplayMember = "Nombre";
            cbxMaestro.ValueMember = "idmaestro";
        }
        private void TraerGrupo(string filtro)
        {
            cbxGrupo.DataSource = _GruposManejador.GetGrupos(filtro);
            cbxGrupo.DisplayMember = "Grupo";
            cbxGrupo.ValueMember = "idgrupo";
        }
        private void TraerMateria(string filtro)
        {
            cbxMateria.DataSource = _MateriasManejador.GetMaterias(filtro);
            cbxMateria.DisplayMember = "NombreMateria";
            cbxMateria.ValueMember = "idmateria";
        }
        private void GuardarAsigancion()
        {
            _AsignacionesManejador.Guardar(new Asignaciones
            {
                Idasignacion = Convert.ToInt32(lblId.Text),
                Maestro = cbxMaestro.SelectedValue.ToString(),
                Grupo= cbxGrupo.SelectedValue.ToString(),
                Materia = cbxMateria.SelectedValue.ToString()
            });
        }

        private void EliminarAsignacion()
        {
            var Id = dgvAsignaciones.CurrentRow.Cells["idasignacion"].Value;
            _AsignacionesManejador.Eliminar(Convert.ToInt32(Id));
        }
        private void ModificarAsigancion()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            lblId.Text = dgvAsignaciones.CurrentRow.Cells["idgrupo"].Value.ToString();
            cbxMaestro.Text = dgvAsignaciones.CurrentRow.Cells["Maestro"].Value.ToString();
            cbxGrupo.Text = dgvAsignaciones.CurrentRow.Cells["Grupo"].Value.ToString();
            cbxMateria.Text = dgvAsignaciones.CurrentRow.Cells["Materia"].Value.ToString();

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAsignacion(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarAsigancion();
                LimpiarCuadros();
                BuscarAsignacion("");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAsignacion();
                    BuscarAsignacion("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvAsignaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAsigancion();
                BuscarAsignacion("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
