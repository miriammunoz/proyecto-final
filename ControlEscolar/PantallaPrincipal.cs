﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarios usuarios = new frmUsuarios();
            usuarios.ShowDialog();
        }

        private void alumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlumnos alumnos = new frmAlumnos();
            alumnos.ShowDialog();
        }

        private void maestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Maestros maestros = new Maestros();
            maestros.ShowDialog();
        }

        private void escuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEscuela escuela = new frmEscuela();
            escuela.ShowDialog();
        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMaterias materia = new frmMaterias();
            materia.ShowDialog();
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGrupos grupos = new frmGrupos();
            grupos.ShowDialog();
        }

        private void asignacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAsignaciones asignacion = new frmAsignaciones();
            asignacion.ShowDialog();
        }

        private void kardexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKardex kardex = new frmKardex();
            kardex.ShowDialog();
        }
    }
}
