﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmGrupos : Form
    {
        private GruposManejador _GruposManejador;
        private SemestresManejador _SemestresManejador;
        private CarrerasManejador _CarrerasManejador;
        private AlumnoManejador _AlumnoManejador;
        public frmGrupos()
        {
            InitializeComponent();
            _GruposManejador = new GruposManejador();
            _SemestresManejador = new SemestresManejador();
            _CarrerasManejador = new CarrerasManejador();
            _AlumnoManejador = new AlumnoManejador();
        }

        private void frmGrupos_Load(object sender, EventArgs e)
        {
            BuscarGrupo("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerSemestre("");
            TraerCarrera("");
        }
        private void BuscarGrupo(string filtro)
        {
            dgvGrupos.DataSource = _GruposManejador.GetGrupos(filtro);
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cbxSemestre.Enabled = activar;
            txtGrupo.Enabled = activar;
            cbxCarrera.Enabled = activar;
            cbxAlumno.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtGrupo.Text = "";
            lblId.Text = "0";
            cbxCarrera.Text = "";
            cbxSemestre.Text = "";
            cbxAlumno.Text = "";
        }
        private void TraerSemestre(string filtro)
        {
            cbxSemestre.DataSource = _SemestresManejador.GetSemestres(filtro);
            cbxSemestre.ValueMember = "idsemestre";
            cbxSemestre.DisplayMember = "NumeroSemestre";
        }
        private void TraerCarrera(string filtro)
        {
            cbxCarrera.DataSource = _CarrerasManejador.GetCarreras(filtro);
            cbxCarrera.DisplayMember = "NombreCarrera";
            cbxCarrera.ValueMember = "idcarrera";
        }
        private void TrearAlumno(string filtro)
        {
            cbxAlumno.DataSource = _AlumnoManejador.GetAlumnos(filtro);
            cbxAlumno.DisplayMember = "nombrealumno";
            cbxAlumno.ValueMember = "Id";
        }
        private void GuardarGrupo()
        {
            _GruposManejador.Guardar(new Grupos
            {
                Idgrupo = Convert.ToInt32(lblId.Text),
                Semestre = cbxSemestre.SelectedValue.ToString(),
                Grupo = txtGrupo.Text,
                Carrera = cbxCarrera.SelectedValue.ToString(),
                Alumno=cbxAlumno.SelectedValue.ToString()
            });
        }

        private void EliminarGrupo()
        {
            var Id = dgvGrupos.CurrentRow.Cells["idgrupo"].Value;
            _GruposManejador.Eliminar(Convert.ToInt32(Id));
        }
        private void ModificarGrupo()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            lblId.Text = dgvGrupos.CurrentRow.Cells["idgrupo"].Value.ToString();
            cbxSemestre.Text = dgvGrupos.CurrentRow.Cells["Semestre"].Value.ToString();
            txtGrupo.Text = dgvGrupos.CurrentRow.Cells["Grupo"].Value.ToString();
            cbxCarrera.Text = dgvGrupos.CurrentRow.Cells["Carrera"].Value.ToString();
            cbxAlumno.Text = dgvGrupos.CurrentRow.Cells["Alumno"].Value.ToString();

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupo(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarGrupo();
                LimpiarCuadros();
                BuscarGrupo("");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupo();
                    BuscarGrupo("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void dgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupo();
                BuscarGrupo("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cbxAlumno_Click(object sender, EventArgs e)
        {
            TrearAlumno("");
        }
    }
}
