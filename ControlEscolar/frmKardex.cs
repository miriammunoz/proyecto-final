﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;
using System.Threading;

namespace ControlEscolar
{
    public partial class frmKardex : Form
    {
        bool terminado = true;
        private AltaMateriasManejador _AltaMateriasManejador;
        private MateriasManejador _MateriasManejador;
        private AlumnoManejador _AlumnoManejador;
        public frmKardex()
        {
            InitializeComponent();
            _MateriasManejador = new MateriasManejador();
            _AltaMateriasManejador = new AltaMateriasManejador();
            _AlumnoManejador = new AlumnoManejador();
        }

        private void frmKardex_Load(object sender, EventArgs e)
        {
            BuscarKardex("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerAlumno("");
            TraerMateria("");
        }
        private void BuscarKardex(string filtro)
        {
            dgvKardex.DataSource = _AltaMateriasManejador.GetAlta(filtro);
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cbxAlumno.Enabled = activar;
            cbxMateria.Enabled = activar;
            txtP1.Enabled = activar;
            txtP2.Enabled = activar;
            txtP3.Enabled = activar;
            txtP4.Enabled = activar;
 
        }
        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            cbxMateria.Text = "";
            cbxAlumno.Text = "";
            txtP1.Text = "0";
            txtP2.Text = "0";
            txtP3.Text = "0";
            txtP4.Text = "0";
        }
        private void TraerMateria(string filtro)
        {
            cbxMateria.DataSource = _MateriasManejador.GetMaterias(filtro);
            cbxMateria.DisplayMember = "NombreMateria";
            cbxMateria.ValueMember = "idmateria";
        }
        private void TraerAlumno(string filtro)
        {
            cbxAlumno.DataSource = _AlumnoManejador.GetAlumnos(filtro);
            cbxAlumno.DisplayMember = "nombrealumno";
            cbxAlumno.ValueMember = "Id";
        }
        private void GuardarKardex()
        {
            _AltaMateriasManejador.Guardar(new AltaMaterias
            {
                Idalta = Convert.ToInt32(lblId.Text),
                Alumno = cbxAlumno.SelectedValue.ToString(),
                Materia = cbxMateria.SelectedValue.ToString(),
                Parcial1 = Convert.ToDouble(txtP1.Text),
                Parcial2 = Convert.ToDouble(txtP2.Text),
                Parcial3 = Convert.ToDouble(txtP3.Text),
                Parcial4 = Convert.ToDouble(txtP4.Text),


            });
        }

        private void EliminarKardex()
        {
            var Id = dgvKardex.CurrentRow.Cells["idalta"].Value;
            _AltaMateriasManejador.Eliminar(Convert.ToInt32(Id));
        }
        private void ModificarKardex()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            lblId.Text = dgvKardex.CurrentRow.Cells["idalta"].Value.ToString();
            cbxAlumno.Text = dgvKardex.CurrentRow.Cells["Alumno"].Value.ToString();
            cbxMateria.Text = dgvKardex.CurrentRow.Cells["Materia"].Value.ToString();
            txtP1.Text = dgvKardex.CurrentRow.Cells["Parcial1"].Value.ToString();
            txtP2.Text = dgvKardex.CurrentRow.Cells["Parcial2"].Value.ToString();
            txtP3.Text = dgvKardex.CurrentRow.Cells["Parcial3"].Value.ToString();
            txtP4.Text = dgvKardex.CurrentRow.Cells["Parcial4"].Value.ToString();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarKardex(txtBuscar.Text);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarKardex();
                LimpiarCuadros();
                BuscarKardex("");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarKardex();
                    BuscarKardex("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvKardex_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            try
            {
                ModificarKardex();
                BuscarKardex("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ExportarDataGridViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplication;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplication = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplication.Workbooks.Add();
                    hoja_trabajo =
                        (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }

                    libros_trabajo.SaveAs(fichero.FileName,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplication.Quit();

                   // MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (!bgwExport.IsBusy)
                    {
                        bgwExport.RunWorkerAsync();
                        this.timer1.Start();
                    }
                }
                
            }
            catch (Exception)
            {
                MessageBox.Show("Fallo ", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvKardex);
        }
        private bool ProcesoCargar()
        {
            try
            {
                for (int i = 0; i < 20; i++)
                {
                    Thread.Sleep(500);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void bgwExport_DoWork(object sender, DoWorkEventArgs e)
        {
            terminado = ProcesoCargar();
        }

        private void bgwExport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (terminado)
            {

                MessageBox.Show("El proseso se completo correctamente");
            }
            else
            {
                MessageBox.Show("EL PROCESO FALLÓ");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.pbBarra.Increment(Convert.ToInt32(terminado));
        }
    }
}
